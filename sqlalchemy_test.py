from uuid import uuid4

from pydantic import BaseModel, Field
from pydantic.types import UUID4
from sqlalchemy import Column, MetaData, Table, create_engine,\
                        text, insert, select
from sqlalchemy.exc import NoResultFound, MultipleResultsFound, NoSuchColumnError
from sqlalchemy.dialects import postgresql
from IPython import embed

DATABASE_NAME = "sqlalchemy_test"

## Drop and recreate the tables each run for a quick development loop
psql_engine = create_engine("postgresql+psycopg2://solar499:solar499@localhost:5432/postgres")
with psql_engine.connect().execution_options(
    isolation_level="AUTOCOMMIT"
) as connection:
    connection.execute(text(f"DROP DATABASE IF EXISTS {DATABASE_NAME}"))
    connection.execute(text(f"CREATE DATABASE {DATABASE_NAME}"))

engine = create_engine(f"postgresql+psycopg2://solar499:solar499@localhost:5432/{DATABASE_NAME}")

## Create a few very simple pydantic models


class Child(BaseModel):
    id: UUID4 = Field(default_factory=uuid4)


class Parent(BaseModel):
    id: UUID4 = Field(default_factory=uuid4)
    children: list[Child]


## Write the schema definitions


metadata_obj = MetaData()

##David here
# I thought it didn't make much sense to have the id of the parent be called
# "user_id". Maybe that was originally a typo.
# Below, the obj_to_row and similar methods make the assumption that
# postgres columns and pydantic attributes have the same name.
# That assumption may not hold depending on what we'll be doing with real
# data but it usually is a good thing to design the database tables (or the
# python models) so that the mapping is 1 to 1.
# If for some reason that is not possible, the advantages of the abstractions
# done below still hold, and it is better to have the column name "translations"
# as an explicit dictionary (pun intended) passed to the obj_to_row and
# row_to_obj methods, while keeping the rest the same.

parent = Table(
    "parent",
    metadata_obj,
    # Column("user_id", postgresql.UUID(), primary_key=True),
    Column("id", postgresql.UUID(), primary_key=True),
)

child = Table(
    "child",
    metadata_obj,
    Column("id", postgresql.UUID(), primary_key=True),
    Column("parent_id", postgresql.UUID()),
)

# Other required tables

metadata_obj.create_all(engine)

##David here
# I'll use this class below to return query results. It imitates
# the most important parts of the sqlalchemy query results api, but
# it is a wrapper around a pydantic models iterable.

class QueryResult:
    def __init__(self, iterable, count):
        self.iterable = (x for x in iterable)
        self.count = count

    def count(self):
        return self.count

    def one(self):
        try:
            result = next(self.iterable)
        except StopIteration:
            raise NoResultFound
        try:
            next(self.iterable)
            raise MultipleResultsFound
        except:
            return result

    def one_or_none(self):
        try:
            return self.one()
        except:
            return None

    def fetchone(self):
        try:
            return next(self.iterable)
        except StopIteration:
            return None

    def all(self):
        return list(self.iterable)

## Write a database socket which can write and query pydantic models
class Socket:
    def __init__(self, model_to_table, data_model):
        self.model_to_table = model_to_table
        self.data_model = data_model

        dm = data_model
        while(True):
            dm["columns"] = model_to_table[dm["model"]].columns
            if "child" in dm.keys():
                dm = dm["child"]
            else:
                break;

    def sub_data_model(self, model):
        dm = self.data_model
        while(True):
            if dm["model"] == model:
                return dm
            if "child" in dm.keys():
                dm = dm["child"]
            else:
                return None

    def column_list(self, model):
        column_list = []
        dm = self.sub_data_model(model)
        while(True):
            column_list += dm["columns"]
            if "child" in dm.keys():
                dm = dm["child"]
            else:
                break;
        return column_list

    def obj_to_row(self, table, obj):
        row = {}
        for col in table.columns:
            try:
                row[col.name] = getattr(obj, col.name)
            except AttributeError:
                pass
        return row

    def row_to_dict(self, data_model, models, row, i):
        attrs = {}
        for col in data_model['columns']:
            attrs[col.name] = row[i]
            i += 1
        primary_key = data_model["primary_key"]
        if attrs[primary_key] not in models.keys():
            models[attrs[primary_key]] = attrs

        if "child" in data_model.keys():
            parent = models[attrs[primary_key]]
            if data_model["child"]["name"] not in parent.keys():
                parent[data_model["child"]["name"]] = {}
            i = self.row_to_dict(data_model["child"], parent[data_model["child"]["name"]], row, i)

        return i

    def dict_to_obj(self, data_model, models):
       for id, model in models.items():
            if "child" in data_model.keys():
                self.dict_to_obj(data_model["child"], model[data_model["child"]["name"]])
                model[data_model["child"]["name"]] = list(model[data_model["child"]["name"]].values())
            models[id] = data_model["model"](**model)

    def create_children(self, data_model, conn, model, parent_objs):
        """Recursively creates children for a list of pydantic objects."""
        if "child" in data_model.keys():
            child_map = data_model["child"]
            child_model = child_map["model"]
            child_table = self.model_to_table[child_model]
            all_children = ({
                    'obj': child_obj,
                    'row': {
                            **self.obj_to_row(child_table, child_obj),
                            child_map["parent_key"]: getattr(obj, child_map["foreign_key"])
                        }
                    }
                    for obj in parent_objs for child_obj in getattr(obj, child_map["name"])
                )
            conn.execute(insert(child_table), [c['row'] for c in all_children])
            self.create_children(child_map, conn, child_model, (c['obj'] for c in all_children))

    def create_many(self, model, objs: list[BaseModel]) -> int:
        """Bulk creates a list of pydantic objects and returns the number inserted."""
        table = self.model_to_table[model]
        with engine.begin() as conn:
            result = conn.execute(
                    insert(table).returning(table.c.id),
                    [self.obj_to_row(table, obj) for obj in objs]
                )
            self.create_children(self.data_model, conn, model, objs)
        return result.rowcount

    def query(self, model, id: UUID4 | list[UUID4]) -> QueryResult:
        """Queries either a single id or multiple ids from the table and returns a QueryResult."""
        table = self.model_to_table[model]
        with engine.begin() as conn:
            ids = id if isinstance(id, list) else [id]

            query = select(*self.column_list(model)).where(table.c.id.in_(ids))
            dm = self.sub_data_model(model)
            while(True):
                if "child" in dm.keys():
                    child = dm["child"]
                    table = self.model_to_table[dm["model"]]
                    child_table = self.model_to_table[child["model"]]
                    query = query.join(
                                child_table,
                                getattr(table.c, child['foreign_key']) == getattr(child_table.c, child['parent_key'])
                            )
                    dm = child
                else:
                    break;

            result = conn.execute(query)

            models = {}
            for row in result:
                i = self.row_to_dict(self.sub_data_model(model), models, row, 0)
            self.dict_to_obj(self.sub_data_model(model), models)
            return QueryResult(models.values(), result.rowcount)

socket = Socket({
        Parent: parent,
        Child: child
    }, {
            "model": Parent,
            "primary_key": "id",
            "child": {
                "name": "children",
                "type": "array",
                "parent_key": "parent_id",
                "foreign_key": "id",
                "primary_key": "id",
                "model": Child,
            }
        })

# Create several children
parent1 = Parent(children=[Child(), Child()])
parent2 = Parent(children=[Child()])
# parent3 = Parent(children=[Child(), parent1.children[0]])
child1 = Child()
parent3 = Parent(children=[Child(), child1])

assert socket.create_many(Parent, [parent1, parent2, parent3]) == 3
assert socket.query(Parent, id=parent1.id).one().id == parent1.id
assert socket.query(Child, id=child1.id).one().id == child1.id
# Write any additional tests as desired!
